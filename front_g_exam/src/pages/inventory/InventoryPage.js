import React, { Component } from 'react';
import axios from 'configs/api.service';
import { Row } from 'antd';
import DisplayInventoryList from 'components/DisplayInventoryList/DisplayInventoryList';
import SearchInventory from 'components/Search/SearchInventory';
import { connect } from 'react-redux';

import styles from './InventoryPage.module.css';
import { handleError } from 'services/handle-error.service';
import { fetchStores, searchStoresQuick } from 'services/redux/actions/actionsCreator';

class InventoryPage extends Component {

  state = {
    storeKeyword: '',
    stores: [],
    storesTable: []
  };

  componentDidMount = async () => {
    await this.fetchStores();
  };

  componentDidUpdate(prevProps, prevState, snapshot) {
    const { stores } = this.props;
    if (prevProps.stores !== stores) this.setStoresTable(stores);
  }

  fetchStores = async () => {
    try {
      await this.props.fetchStores();
      this.setState({ stores: this.props.stores });
      this.setStoresTable(this.props.stores);
    } catch (err) {
      const error = handleError(err);
      console.error(err);
      console.error('Error ❌ ', error.message);
    }
  };

  setStoresTable = stores => {
    this.setState({
      storesTable: stores ? stores.map(store => {
        const { id, product, quantity, branch } = store;
        return {
          key: id,
          id: id,
          productName: product.name,
          price: product.price,
          quantity: quantity,
          branch: branch.name,
          tag: quantity <= 500 ? 'LESS' : quantity <= 2000 ? 'NORMAL' : 'MORE'
        }
      }) : []
    });
  };

  handleSearchStoresQuick = async (keyword) => {
    this.setState({ storeKeyword: keyword });
    try {
      await this.props.searchStoresQuick(keyword);

      this.setStoresTable(this.props.stores);
    } catch (err) {
      const error = handleError(err);
      console.error(err);
      console.error('Error ❌ ', error.message);
    }
  };

  handleRemoveStore = async (storeId) => {
    const { storeKeyword } = this.state;
    try {
      await axios.delete(`http://localhost:8080/api/v1/stores/${storeId}`);
      await (storeKeyword ? this.handleSearchStoresQuick(storeKeyword) : this.fetchStores());
    } catch (err) {
      const error = handleError(err);
      console.error(err);
      console.error('Error ❌ ', error.message);
    }
  };

  render() {
    const { stores, storesTable } = this.state;

    return (
      <>
        <div>
          <h1>Inventory Page</h1>
          <Row type="flex" justify="center" className={styles.Row}>
            <SearchInventory onSearchStores={this.handleSearchStoresQuick} stores={stores} />
          </Row>
          <Row type="flex" justify="center" className={styles.Row}>
            <DisplayInventoryList onFetchStores={this.props.fetchStores} onRemoveStore={this.handleRemoveStore}
                                  storesTable={storesTable} stores={stores} />
          </Row>
        </div>
      </>
    );
  }
}

const mapStateToProps = state => {
  return {
    stores: state.stores
  }
};

const mapDispatchToProps = {
  fetchStores,
  searchStoresQuick: searchStoresQuick
};

// const mapDispatchToProps = dispatch => {
//   return {
//     fetchStores: dispatch(fetchStores())
//   }
// };

export default connect(mapStateToProps, mapDispatchToProps)(InventoryPage);
