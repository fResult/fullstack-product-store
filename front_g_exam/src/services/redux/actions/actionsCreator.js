import axios from 'configs/api.service';
import { storesTypes, storeTypes } from './actionTypes';

export const fetchStores = () => async dispatch => {
  dispatch({
    type: storesTypes.FETCH_STORES,
    stores: (await axios.get('/stores')).data
  })
};

export const searchStoresQuick = (keyword) => async dispatch => {
  dispatch({
    type: storesTypes.SEARCH_STORES_QUICK,
    stores: (await axios.get(`/stores/quick-search?productName=${keyword}&branchName=${keyword}`)).data
  })
};

export const searchStoresAdvance = (storeParams) => async dispatch => {
  const { productName, branch, minPrice, maxPrice, quantity } = storeParams;
  dispatch({
    type: storesTypes.SEARCH_STORES_ADVANCE,
    stores: (await axios.get(`/stores/advance-search?productName=${productName}&branchName=${branch}&minPrice=${minPrice}&maxPrice=${maxPrice}&quantity=${quantity}`)).data
  })
};

export const selectStore = (stores, storeId) => ({
    type: storeTypes.SELECT_STORE,
    stores: stores,
    storeId: storeId
});
