import React, { useEffect, useState } from 'react';
import { Button, Col, Divider, Input, Row, Typography } from 'antd';
import ModalSearchInventory from 'components/ModalSearchInventoryForm/ModalSearchInventory';

import { WAIT_INTERVAL, ENTER_KEY } from 'services/input.service';

import styles from './SearchInventory.module.css';

const SearchInventory = props => {

  // const [keyword, setKeyword] = useState('');
  const [isSearchModalVisible, setIsSearchModalVisible] = useState(false);
  const [loading, setLoading] = useState(false);

  let timer = null;

  useEffect(() => {
    return () => timer = null;
  });

  const handleChangeSearch = e => {
    clearTimeout(timer);
    timer = setTimeout(triggerChange.bind(null, e.target.value), WAIT_INTERVAL);
  };

  const handleKeyUp = (e) => {
    if (e.keyCode === ENTER_KEY) {
      triggerChange.bind(null, e.target.value);
    }
  };

  const triggerChange = (value) => {
    setLoading(true);
    props.onSearchStores(value);
    setLoading(false);
  };

  const handleShowSearchModal = (isSearchModalVisible) => {
    setIsSearchModalVisible(isSearchModalVisible);
  };

  return (
    <>
      <div style={{ marginTop: '70px' }}>
        <Row type="flex" gutter={5} className={styles.RowSearch}>
          <Col>
            <Typography.Title level={4}>Quick Search</Typography.Title>
            <Input.Search style={{ width: 350 }} size="large"
                          onChange={handleChangeSearch}
                          onKeyUp={handleKeyUp}
                          loading={loading}
                          placeholder="Search Product or Branch"
            />
          </Col>
        </Row>
        <Divider dashed className={styles.Divider} />
        <Row type="flex" justify="center" className={styles.RowSearch}>
          <Button type="primary" size="large" onClick={() => handleShowSearchModal(true)}>
            Open Advance Search window
          </Button>
        </Row>

        <ModalSearchInventory loading={loading} isSearchModalVisible={isSearchModalVisible}
                              onShowSearchModal={handleShowSearchModal} stores={props.stores}
        />
      </div>
    </>
  );
};

export default SearchInventory;
