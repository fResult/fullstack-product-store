import React, { useEffect, useState } from 'react';
import { Button, Form, Modal, Select, Slider } from 'antd';
import axios from 'configs/api.service';
import { connect } from 'react-redux';

import styles from './ModalSearchInventory.module.css';
import { searchStoresAdvance } from "services/redux/actions/actionsCreator";
import { handleError } from "services/handle-error.service";

const { Option } = Select;

const ModalSearchInventory = props => {
  const [minPrice, setMinPrice] = useState(9000);
  const [maxPrice, setMaxPrice] = useState(21000);
  const [quantity, setQuantity] = useState(500);
  const [, setProduct] = useState('');
  const [, setBranch] = useState('');
  const [branches, setBranches] = useState([]);
  const [products, setProducts] = useState([]);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    (async () => fetchProducts())();
    (async () => fetchBranches())();
  }, []);

  const fetchProducts = async () => {
    const products = (await axios.get('/products')).data;
    setProducts(products);
  };

  const fetchBranches = async () => {
    const branches = (await axios.get('/branches')).data;
    setBranches(branches);
  };

  const handleSearchStoresAdvance = e => {
    e.preventDefault();
    props.form.validateFieldsAndScroll(async (err, values) => {
      const { productName, branch } = values;
      if (!err) {
        setLoading(true);
        console.info('Received values of form: ', values);
        try {
          await props.searchStoresAdvance({ productName, branch, minPrice, maxPrice, quantity });
          props.form.resetFields();
          props.onShowSearchModal(false);
        } catch (err) {
          const error = handleError(err);
          console.error(err);
          console.error('Error ❌ ', error.message);
        }
        setLoading(false);
      }
    });
  };

  const handleClearForm = () => {
    props.form.resetFields();
  };

  const handleChangePriceRange = value => {
    setMinPrice(value[0] * 300);
    setMaxPrice(value[1] * 300);
  };

  const { form } = props;

  const tailFormItemLayout = {
    wrapperCol: {
      xs: {
        span: 24,
        offset: 0,
      },
      sm: {
        span: 16,
        offset: 8,
      },
    },
  };

  const marks = {
    0: '0', 50: '15,000', 100: '30,000'
  };

  const branchOptions = branches && branches.map(branch => (
    <Option key={branch.name} value={branch.name}>{branch.name}</Option>)
  );
  const productOptions = products && products.map(product => (
    <Option key={product.name} value={product.name}>{product.name}</Option>
  ));

  return (
    <>
      <Modal title="Advance Search Inventory" style={{ top: 20 }}
             visible={props.isSearchModalVisible}
             onOk={() => props.onShowSearchModal(false)}
             onCancel={() => props.onShowSearchModal(false)}
      >
        <h1>Search Inventory Form Modal</h1>
        <Form labelCol={{ xs: { span: 24 }, sm: { span: 8 } }}
              wrapperCol={{ xs: { span: 24 }, sm: { span: 16 }, }}
              onSubmit={handleSearchStoresAdvance}
        >
          <Form.Item label="Product Name">
            {form.getFieldDecorator('productName', { initialValue: '' })(
              <Select onChange={setProduct}>
                {productOptions}
              </Select>
            )}
          </Form.Item>
          <Form.Item label="Branch">
            {form.getFieldDecorator('branch', { initialValue: '' })(
              <Select onChange={setBranch}>
                {branchOptions}
              </Select>
            )}
          </Form.Item>
          <Form.Item label="Price">
            {form.getFieldDecorator('price', { initialValue: [30, 70] })(
              <Slider range marks={marks} tipFormatter={value => value * 300}
                      onChange={handleChangePriceRange}
              />
            )}
          </Form.Item>
          <Form.Item label="Quantity (Less than)">
            {form.getFieldDecorator('quantity', { initialValue: 10 })(
              <Slider
                marks={{
                  0: { style: { color: '#FA541C', fontSize: 13 }, label: 0 },
                  10: { style: { color: '#FA541C', fontSize: 13 }, label: 500 },
                  20: { style: { color: 'rgba(0, 0, 0, 0.65)', fontSize: 13 }, label: 1000 },
                  40: { style: { color: 'rgba(0, 0, 0, 0.65)', fontSize: 13 }, label: 2000 },
                  50: { style: { color: '#52C41A', fontSize: 13 }, label: 2500 },
                  75: { style: { color: '#52C41A', fontSize: 13 }, label: 3750 },
                  100: { style: { color: '#52C41A', fontSize: 13 }, label: 5000 }
                }}
                tipFormatter={value => value * 50}
                step={null}
                onChange={value => setQuantity(value * 50)} />
            )}
          </Form.Item>
          <Form.Item {...tailFormItemLayout}>
            <Button className={styles.ButtonModal} onClick={handleClearForm}>Clear</Button>
            <Button type="primary" htmlType="submit" loading={loading} className={styles.ButtonModal}>
              Search
            </Button>
          </Form.Item>
        </Form>
      </Modal>
    </>
  );
};

const mapStateToProps = state => {
  return {
    stores: state.stores
  }
};

const mapDispatchToProps = {
  searchStoresAdvance
};

const ModalSearchInventoryForm = Form.create({ name: 'SearchInventoryForm' })(ModalSearchInventory);
export default connect(mapStateToProps, mapDispatchToProps)(ModalSearchInventoryForm);
