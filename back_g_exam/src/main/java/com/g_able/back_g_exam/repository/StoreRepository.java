package com.g_able.back_g_exam.repository;

import com.g_able.back_g_exam.entity.Store;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StoreRepository extends JpaRepository<Store, Integer> {

  //  List<Store> findStoresByProductAndBranch(Product product, Branch branch);

  @Query(nativeQuery = true,
      value = " SELECT * " +
          "FROM stores s " +
          "INNER JOIN products p " +
          "ON p.id = s.product_id " +
          "INNER JOIN branches b " +
          "ON b.id = s.branch_id " +
          "WHERE (p.name LIKE CONCAT('%', :productName , '%') OR :productName IS NULL) " +
          " OR (b.name LIKE  CONCAT('%', :branchName , '%')  OR :branchName IS NULL)")
  List<Store> findStoresByProductNameOrBranchName(@Param("productName") String productName, @Param("branchName") String branchName);

  @Query(nativeQuery = true,
      value = "SELECT * " +
          "FROM stores s " +
          "INNER JOIN products p " +
          "ON p.id = s.product_id " +
          "INNER JOIN branches b " +
          "ON b.id = s.branch_id " +
          "WHERE (p.name LIKE CONCAT('%', :productName , '%') OR :productName IS NULL) " +
          " AND (b.name LIKE  CONCAT('%', :branchName , '%')  OR :branchName IS NULL) " +
          " AND (:minPrice <= p.price AND p.price <= :maxPrice) " +
          " AND s.quantity <= :quantity " +
          " ORDER BY p.name, b.name, s.quantity")
  List<Store> searchStoresByFilter(@Param("productName") String productName, @Param("branchName") String branchName,
                                 @Param("minPrice") double minPrice, @Param("maxPrice") double maxPrice, @Param("quantity") int quantity);


}
