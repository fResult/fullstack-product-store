package com.g_able.back_g_exam;

import com.g_able.back_g_exam.entity.Branch;
import com.g_able.back_g_exam.entity.Product;
import com.g_able.back_g_exam.entity.Store;
import com.g_able.back_g_exam.repository.*;
import com.github.javafaker.Faker;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@RequiredArgsConstructor
//@AllArgsConstructor
@SpringBootApplication
public class BackGExamApplication implements CommandLineRunner {

  private final ProductRepository productRepository;
  private final BranchRepository branchRepository;
  private final StoreRepository storeRepository;

  public static void main(String[] args) {
    SpringApplication.run(BackGExamApplication.class, args);
  }

  @Override
  public void run(String... args) {

    init50Products();
    init10Branches();
    initManyStores();

  }

  public void init50Products() {
    Faker faker = new Faker();
    List<Product> products = new ArrayList<>();

    for (int i = 1; i <= 50; i++) {
      String name = faker.commerce().productName();
      String detail = faker.lorem().sentence(5);
      String price = faker.commerce().price(3000, 30000);
//      double price = Math.ceil(Math.random() * 30_000);
      Product product = Product.builder().name(name).price(Double.parseDouble(price)).detail(detail).build();
      products.add(product);
    }

    productRepository.saveAll(products);
  }

  public void init10Branches() {

    Faker faker = new Faker();
    List<Branch> branches = new ArrayList<>();

    for (int i = 1; i <= 10; i++) {
      String name = faker.address().city();
      Branch branch = Branch.builder().name(name).build();
      branches.add(branch);
    }

    branchRepository.saveAll(branches);
  }

  public void initManyStores() {
    List<Store> stores = new ArrayList<>();
    for (int bid = 1, sid = 1; bid <= 10; bid++, sid++) {
//      (Math.floor(Math.random() * 50) < 20 ? 21 : pid)
      for (int pid = (int) Math.ceil(Math.random() * 50); pid <= 50; pid += Math.ceil(Math.random() * 7)) {
        Store store = Store.builder()
            .product(new Product(pid, null, null, 0, null))
            .branch(new Branch(bid, null, null))
            .quantity((int) Math.ceil(Math.random() * 2000)).build();
        stores.add(store);
      }
    }

    storeRepository.saveAll(stores);
  }

}
