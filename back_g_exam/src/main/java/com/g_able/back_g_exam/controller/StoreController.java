package com.g_able.back_g_exam.controller;

import com.g_able.back_g_exam.controller.common.CommonController;
import com.g_able.back_g_exam.entity.Branch;
import com.g_able.back_g_exam.entity.Store;
import com.g_able.back_g_exam.params_model.ProductParams;
import com.g_able.back_g_exam.service.StoreService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@AllArgsConstructor
public class StoreController extends CommonController {
  private final StoreService storeService;

  @GetMapping("/stores")
  public List<Store> findStores() {
    return storeService.findStores();
  }

  @GetMapping("/stores/quick-search")
  public ResponseEntity<List<Store>> findStoreByQuickSearch(@RequestParam(required = false) String productName,
                                                            @RequestParam(required = false) String branchName) {

    List<Store> stores = storeService.findStoresByProductNameOrBranchName(productName, branchName);
    if (stores.size() == 0) return ResponseEntity.noContent().build();
    return ResponseEntity.ok(stores);
  }

  @GetMapping("/stores/advance-search")
  public List<Store> findStoresByFilter(
      @RequestParam(required = false) String productName, @RequestParam(required = false) String branchName,
      @RequestParam(required = false, defaultValue = "0") Integer minPrice,
      @RequestParam(required = false, defaultValue = "999999") Integer maxPrice,
      @RequestParam(required = false, defaultValue = "3000") Integer quantity) {

    ProductParams productParams = ProductParams.builder().name(productName).minPrice(minPrice).maxPrice(maxPrice).build();
    Branch branch = Branch.builder().name(branchName).build();

    List<Store> stores = storeService.findStoresByFilter(productParams, branch, quantity);
    System.out.println("Stores: " + stores);
    return stores;

  }

  @PostMapping("/stores")
  public Store createStore(@RequestBody Store store) {
    return null;
  }

  @PatchMapping("/stores/{storeId}")
  public ResponseEntity<?> updateStoreQuantityById(@PathVariable Integer storeId, @RequestBody Store store) {
    try {
      Optional<Store> optStore = storeService.updateStoreById(storeId, store);
      if (!optStore.isPresent())
        return ResponseEntity.noContent().build();

      return ResponseEntity.ok("Update Store ID: " + store.getId() + " successfully");
    } catch (Exception ex) {
      return ResponseEntity.badRequest().body(ex.getMessage());
    }
  }

  @DeleteMapping("/stores/{storeId}")
  public ResponseEntity<?> deleteStoreById(@PathVariable Integer storeId) {
    if (!storeService.deleteStoreById(storeId)) {
      return ResponseEntity.noContent().build();
    }
    return ResponseEntity.ok("Remove Store ID: " + storeId + " successfully");
  }

}
