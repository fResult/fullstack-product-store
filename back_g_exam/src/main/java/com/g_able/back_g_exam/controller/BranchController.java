package com.g_able.back_g_exam.controller;

import com.g_able.back_g_exam.controller.common.CommonController;
import com.g_able.back_g_exam.entity.Branch;
import com.g_able.back_g_exam.service.BranchService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@AllArgsConstructor
public class BranchController extends CommonController {

  private final BranchService branchService;

  @GetMapping("/branches")
  public List<Branch> findBranches() {
    return branchService.findBranches();
  }

  @GetMapping("/branches/{id}")
  public Branch findBranchById(@PathVariable Integer id) {
    System.out.println("findBranchById");
    return null;
  }

  @PostMapping(value = "/branches")
  public Branch createBranch(@RequestBody Branch branch) {
    System.out.println("createBranch");
    return null;
  }

  @PutMapping("/branches/{id}")
  public Branch updateBranchById(@PathVariable Integer id, @RequestBody Branch branch) {
    System.out.println("updateBranchById");
    return null;
  }

  @PatchMapping("/branches/{id}")
  public Branch updatePartialBranchById(@PathVariable Integer id, @RequestParam("branchName") String name) {
    System.out.println("updatePartialBranchById");
    return null;
  }

  @DeleteMapping("/branches/{id}")
  public Branch deleteBranchById(@PathVariable Integer id) {
    System.out.println("deleteBranchById");
    return null;
  }

}
