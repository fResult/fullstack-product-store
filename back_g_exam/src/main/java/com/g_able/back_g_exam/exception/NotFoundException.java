package com.g_able.back_g_exam.exception;

import com.g_able.back_g_exam.exception.CommonException;
import org.springframework.http.HttpStatus;

public class NotFoundException extends CommonException {

  private final HttpStatus STATUS = HttpStatus.NOT_FOUND;
  private final String CODE = "404";

  public NotFoundException(String message) {
    super(message);
  }

}
