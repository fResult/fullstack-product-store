package com.g_able.back_g_exam.exception;

import org.springframework.http.HttpStatus;

public class NoContentException extends CommonException {

  private final HttpStatus STATUS = HttpStatus.NO_CONTENT;
  private final String CODE = "204";

  public NoContentException(String message) {
    super(message);
  }

}
