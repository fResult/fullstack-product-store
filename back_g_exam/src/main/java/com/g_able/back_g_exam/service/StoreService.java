package com.g_able.back_g_exam.service;

import com.g_able.back_g_exam.entity.Branch;
import com.g_able.back_g_exam.entity.Product;
import com.g_able.back_g_exam.entity.Store;
import com.g_able.back_g_exam.exception.NotFoundException;
import com.g_able.back_g_exam.params_model.ProductParams;
import com.g_able.back_g_exam.repository.StoreRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;


@Service
@Transactional
@RequiredArgsConstructor
public class StoreService {
  private final StoreRepository storeRepository;
  private final ProductService productService;
  private final BranchService branchService;

  public List<Store> findStores() {
    return storeRepository.findAll();
  }

  public List<Store> findStoresByProductNameOrBranchName(String productName, String branchName) {
    return storeRepository.findStoresByProductNameOrBranchName(productName, branchName);
  }

  public List<Store> findStoresByFilter(ProductParams productParams, Branch branch, int quantity) {
    return storeRepository.searchStoresByFilter(productParams.getName(), branch.getName(),
        productParams.getMinPrice(), productParams.getMaxPrice(), quantity);
  }

  public boolean deleteStoreById(Integer storeId) {
    try {
      storeRepository.deleteById(storeId);
      return true;
    } catch (EmptyResultDataAccessException ex) {
      return false;
    }
  }

  public Optional<Store> updateStoreById(Integer storeId, Store store) {
    Optional<Store> optStore = storeRepository.findById(storeId);
    if (!optStore.isPresent()) return optStore;

    Optional<Product> optProduct = productService.findProductById(store.getProduct().getId());
    Optional<Branch> optBranch = branchService.findBranchById(store.getBranch().getId());

    Product product = store.getProduct();
    Branch branch = store.getBranch();

    if (optProduct.isPresent()) {
      productService.updateProduct(product.getId(), product);
    } else {
      throw new NotFoundException("products.id = " + product.getId() + " is not found, Transactional is roll back");
    }

    if (optBranch.isPresent()) {
      branchService.updateBranch(branch.getId(), branch);
    } else {
      throw new NotFoundException("branches.id = " + branch.getId() + " is not found, Transactional is roll back");
    }

    store.setId(storeId);
    optStore = Optional.of(storeRepository.save(store));
    store = optStore.get();

    if (store.getQuantity() < 0) {
      throw new NumberFormatException("Product Quantity must not less than 0");
    } else if (store.getQuantity() == 0) {
      storeRepository.deleteById(optStore.get().getId());
      optStore = Optional.empty();
    }

    return optStore;
  }
}
