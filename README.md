Inventory Management
==
📖 Description
--
It is a project that I do follow by G-Able company's assignment  
Backend: Spring Boot 2.3  
Frontend: ReactJS  
CSS Framework: Ant Design V.3

💪 Features
--
* Display stores with pagination
* Quick search and Advance search
* Create new store
* Edit and remove store (Add or subtract product quantity and when quantity is 0 then store will be removed)

😀 Author:
--
Sila Setthakan-anan (fResult)
